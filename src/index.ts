import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

async function getMaxSourceId() {
  const sqlStmt = 'SELECT MAX(source_id) AS max_source_id FROM user_login_archive;';
  const result = await prisma.$queryRaw(sqlStmt);
  console.log(result);
}

async function main() {
  const result = await prisma.$queryRaw('SELECT COUNT(*) FROM user;')
  console.log(result);
  await getMaxSourceId();
}

main();
